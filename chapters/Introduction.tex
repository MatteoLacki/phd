%!TEX root = ../phd.tex
\chapter{Introduction}\label{chap::preliminaries}


\epigraph{``Now this is not the end. It is not even the beginning of the end. But it is, perhaps, the end of the beginning.''}{--- \textup{Winston Churchill}}

\lettrine[lines=4]{\color{BrickRed}M}{ass Spectrometry} is a subfield of the Analytical Chemistry that studies and develops instruments useful for analysing the molecular content of samples.
The instruments, that are called mass spectrometers, have been developed by Joseph J. Thomson just before the First World War and used to study the presence of the isotopes of natural elements~\citep{thomson1913bakerian}.
The output of a mass spectrometer -- a \textit{mass spectrum} -- is a histogram: each bar has its own specific position in the mass-to-charge domain and height equal to that of the observed intensity.
The intensities are usually assumed to be proportional to the number of ions. 
Below, we show how a mass spectrum might look like.
\begin{figure}[h]
  \centering
  \includegraphics[width=.9\textwidth]{spectrum.png}
\end{figure}

Let us study the case of human insulin, \ce{C520H817N139O147S8}, to see how complex can a signal be, even in case of one single source.
the signal even generated by one source might be complex.
To start with, all atoms in the above formula can assume one of multiple isotopic variants, from a set that is different for each element.
Finding a particular isotope in nature is largely a random event.
This does not mean that it is not predictable: when together in large numbers, they do follow many well studied patterns.
The International Union for Pure and Applied Chemistry (IUPAC) is performing continuous measurements of the frequencies of natural isotopes.
Table~\ref{IUPAC} summarizes a small fraction of their findings up till year 2014.

From the viewpoint of statistical modelling, the abundances reported in Table~\ref{IUPAC} represent the probabilities of finding a particular isotope for one given atom of a given element.
By far the easiest way to construct a joint probability measure out of these marginals is to assume that isotopic variants of different atoms are mutually independent.
This assumption dates back to the `60-ies~\citep{beynon1960mass}. 
Of course, in certain particular situations, such as isotopic labelling, one would certainly have to modify that asumption, as these 
could in introduce some non-trivial dependence between the isotopic variants.
In general, however, it would be difficult to come up with a theoretical mechanism that could result in a significant departure from the independence asumption.
Experimental findings does not reject that hypothesis too.

\begin{table}[t]
\caption[Masses and Frequencies of isotopes of elements that make up proteins.]{Masses and Frequencies of isotopes of elements that build up the proteins~\citep{IUPAC}.}\label{IUPAC}
\centering
{\begin{tabular}{lcc}
  {\Futura Isotope}            & {\Futura Mass}   & {\Futura Frequency} \\
  \toprule
  \ce{^1 H}           & 1.0078  & 0.9999 \\
  \ce{^2 H} (\ce{D})  & 2.0141  & 0.0001 \\\midrule
  \ce{^{12} C}        & 12.0000 & 0.9892 \\
  \ce{^{13} C}        & 13.0034 & 0.0108 \\\midrule
  \ce{^{14} N}        & 14.0031 & 0.9964 \\
  \ce{^{15} N}        & 15.0001 & 0.0036 \\\midrule
  \ce{^{16} O}        & 15.9949 & 0.9976 \\
  \ce{^{17} O}        & 16.9991 & 0.0004 \\
  \ce{^{18} O}        & 17.9992 & 0.0021 \\\midrule
  \ce{^{32} S}        & 31.9721 & 0.9499 \\
  \ce{^{33} S}        & 32.9715 & 0.0075 \\
  \ce{^{34} S}        & 33.9679 & 0.0425 \\
  \ce{^{36} S}        & 35.9671 & 0.0001 \\\bottomrule
\end{tabular}
}\end{table}
Mass spectrometer does not distinguish compounds with the same number of isotopic variants.
For instance, if one of the 817 hydrogen atoms of human insuline is deuterium, then, based on mass spectrum alone, there is no way of telling which particular atom was the heavier one.
The observed signal depends only upon counts of different isotopic variants. 
Similarly to how chemical formulas such as \ce{C_c H_h N_n O_o S_s} abstract from spatial composition, we may introduce a more detailed description of the isotopic content of a molecule, such as 
$${\tt iso} = \ce{^{12}C_{c_0} ^{13}C_{c_1} ^{1}H_{h_0} ^{2}H_{h_1} ^{14}N_{n_0} ^{15}N_{n_1} ^{16}O_{o_0} ^{17}O_{o_1} ^{18}O_{o_2} ^{32}S_{s_0} ^{33}S_{s_1} ^{34}S_{s_2} ^{36}S_{s_3} }.$$
Above, $\text{c}_\text{0}$ stands for the number of \ce{^{12}C} isotopes within the molecule, $\text{c}_\text{1}$ -- number of \ce{^{13}C} isotopes, and so on. This is essentially what we call an \textit{isotopologue}.
This definition coincides with that provided by the International Union of Pure and Applied Chemistry \citep{IUPACisotopologue}.
The independence assumptions suggest that the probability of observing an isotopologue is that of a product of multinomial distributions, each for one element, or
\begin{gather*}
p_{\tt iso} = \binom{c}{c_0, c_1} \mathbb{P}\big(^{12}\text{C}\big)^{c_0} \mathbb{P}\big(^{13}\text{C}\big)^{c_1}
\binom{h}{h_0, h_1} \mathbb{P}\big(^{1}\text{H}\big)^{h_0} \mathbb{P}\big(\text{D}\big)^{h_1}
\binom{n}{n_0, n_1} \mathbb{P}\big(^{14}\text{N}\big)^{n_0} \mathbb{P}\big(^{15}\text{N}\big)^{n_1}\\ 
\times \binom{o}{o_0, o_1, o_2} \mathbb{P}\big(^{16}\text{O}\big)^{o_0} \mathbb{P}\big(^{17}\text{O}\big)^{o_1} \mathbb{P}\big(^{18}\text{O}\big)^{o_2}
\binom{o}{o_0, o_1, o_2} \mathbb{P}\big(^{32}\text{S}\big)^{s_0} \mathbb{P}\big(^{33}\text{S}\big)^{s_1} \mathbb{P}\big(^{34}\text{S}\big)^{s_2} \mathbb{P}\big(^{36}\text{S}\big)^{s_3}. 
\end{gather*}
The mass of ${\tt iso}$ is given by mutliplying counts of different isotopes times their masses (Table~\ref{IUPAC}), $m_{\tt iso} = m(^{12}\text{C}) c_0 + m(^{13}\text{C}) c_1 + \dots + m(^{36}\text{S}) s_3$.
The set of all pairs $(p_{\tt iso}, m_{\tt iso})$ that corresponds to one chemical formula \ce{C_c H_h N_n O_o S_s} makes up the \textit{isotopic fine structure}. 
The isotopic fine structure is typically used directly to model the signal in the instrument. 


\subsection*{Isotopic Calculations}

Assume that a chemical compound is made up of elements $\mathcal{E}$, each occuring as $n_e$ atoms with possible $i_e$ isotopic variants.
Then, especially for bigger molecules with $n_e \gg 0$, it does not make any sense to generate the set of all isotopologues, as it comprises $\prod_{e \in \mathcal{E}} \binom{n_e + i_e - 1}{n_e}$ elements.
Using the Stirling's formula, we note that this is an expression of order $\mathcal{O}(\prod_{e \in \mathcal{E}} n_e^{i_e-1})$.
For example, the isotopic fine structure of human insulin comprises more than $10^{14}$ different elements, requiring terabytes of storage.
By far, it is also by far not the biggest known chemical compound.
We are also bounded by the instrumental physics, such as detection thresholds, finite resolution, and limitiations in terms of numbers of ions inside the spectrometer\footnote{Ions have the same charge and repell each other, diverging from their predictable trajectories inside the mass spectrometer.}.
All these factors severly limit the number of observed isotopologues, questioning the need to perform the above calculations.
However, if take into considerations also the probability distribution, then only 1~716 most probable isotopologues represent 99\% of all the probability mass, 5~403 represent 99.9\% of probability, and 13~101 -- 99.99\%.
This means, that it is beneficial to search for a smaller set of configurations with a probability coverage we could control.
\begin{definition*}
    For a given compound, the optimal $P$-set is the smallest set consisting of the most probable peaks of the fine isotopic distribution whose joint probability surpasses $P$. In case of more than one such set, we choose any representative of that class.
\end{definition*}

Chapter~\ref{chap::IsoSpec} describes a particularly efficient and elegant way to quickly generate high coverage subsets of the isotopic fine structure -- the \IsoSpeC algorithm \citep{lacki2017isospec}.
The algorithm makes use of two fundamental features of the multinomial distibution: (1) measure concentration around its mean \citep{giannopoulos2000concentration}, and (2) unimodality \citep{Finucan1964}.
Generally speaking, measure concentration implies that relative few configurations bear most of the probability mass.
To define unimodality in the context of a discrete distribution, we must first define the relationship of neighbourhood between its configurations.
With that at hand, it can be restated in terms of connectedness of the set of local probability maxima.
The unimodality is crucial for the algorithm to work fast with minimal additional data structures: enumerating configurations of the multinomial distribution can be carried out by a \textit{hill descent}.
The two above properties do \textit{tensorize}, i.e. are retained while considering products of distributions.

In Chapter~\ref{chap::IsoSpec} we prove that \IsoSpeC has the optimal, linear time complexity of isotopologues generation.
In the proof, we apply the \textit{Central Limit Theorem} to approximate the number of elements inside an optimal $P$-set by
\begin{equation*}
    M = \frac{q_{\chi^2(k)}(P)^\frac{k}{2}}{C} \frac{\pi^{k/2}}{\Gamma(k/2+1)}\sqrt{ \prod_{e\in\mathcal{E}} \Big(n_e^{i_e - 1} \prod_{j=0}^{i_e-1} \tilde{d}_{ej} } \Big).
\end{equation*}
It results, that the number of elements inside an optimal $P$-set is of order $\mathcal{O}(\sqrt{\prod_{e\in\mathcal{E}}n_e^{i_e - 1}})$.
This is half the degree of number of all isotopologues in the fine isotopic structure.
What is more, the implementation of the algorithm significantly outperforms other existing isotopic calculators. 
The applications of the ideas that resulted in this algorithm go beyond mass spectrometry, and their use is now investigated in statistics and stochastic simulation.


\subsection*{Deconvolution of Mass Spectra \& Ion Statistics}

The isotopic fine structure only describes the isotopic variants of roughly one molecule, while the intensity observed in a mass spectrometer is a function of a relatively high number of ions.
If we assume, that ions reach the detector independently and in large numbers, than the signal of one substance, normalized by the sum of intensities, should be approximately proportional to the isotopic distribution we describe, which follows from the \textit{law of large numbers}.
The above fact is used in many algorithms that perform signal \textit{deisotopisation} -- a procedure that aims at tracing all isotopologues of one substance in a given mass spectrum.
When the potential sources of signal are known in advance, as while performing a database search, one can use methods of nonnegative regression~\citep{slawski2012isotope} that we will describe in Chapter~\ref{chap::MassTodon}.

However, a more detailed approach to the problem, i.e. one that takes into account randomness in ion statistics, can provide interesting insights into the number of observed ions.
It has been theoretically argued that the distribution of the number of ions reaching the detector should follow a Poisson distribution~\citep{Ipsen2012,Ipsen2015}.
This argument goes as follows: assumes that ions move independently throughout the instrument with a limited chance of reaching their final destination.
Then, the number of successful detections is binomial. 
The probability that a sample ion will ever reach the detector is very small, so the binomial distribution is well approximated by the Poisson distribution, which is sometimes reffered to as the \textit{law of rare events}.
Chapter~\ref{chap::MassOn} describes our attempt at merging the concepts of the isotopic distribution with the Poissonian ion statistics -- a tool we call \MassOn.

The model we propose in \MassON also tackles two other important problems in signal processing: (1) the deconvolution of a compound signal and (2) the estimation of the number of observed ions.
The nature of that first problem lies in the limited capability of a mass spectrometer to resolve close mass-to-charge ratios.
In particular, more than one group of isotopologues can be represented by one peak. This is schematically visualized in Figure~\ref{fig::ball spectrum}.
\begin{figure}[th]
    \centering
    \includegraphics[width=.8\linewidth]{ball_spectrum}
    \caption[Idealized convolution]{
    A schematic representation of the convolution of two isotopic distributions. 
    Each ball represents one ion, either of kind A or kind B. 
    The above pattern is typically found in problems where two formulas differ by exactly one hydrogen atom, as that difference shifts the spectrum by around 1 dalton. 
    If the $\frac{m}{z}$ is the ratio of the lightest isotope, then other isotopologues tend to cluster around $\frac{m+k}{z}$, where $k \in \mathbb{N}$.
    This can be attributed to the number of protons inside the nucleai of atoms that make up the molecule.}\label{fig::ball spectrum}
\end{figure}

The second problem stems from the fact that most of the instruments record the ion current that is deemed proportional to the number of passing ions, at least within their trusted dynamic range. % TODO: ADD REFERENCE. 
The problem of estimating the above proportionality factor is of great relevance, as it appears in most of expressions involving the standard deviations of statistics derived from the theoretical mass spectrum. 
In particular, if one assumes that the recorded peak heights truly results from an independent motion of ions close to the detector, then the standard devation of that peak is a function of the square root of the overall number of ions.
Both Chapters~\ref{chap::MassTodon} and \ref{chap::ETDetective} describe other important statistics that rely on the specification of the recorded number of ions \footnote{The problem of estimating that number seems also to be a preliminary step to the much more complex problem of the estimation of the molar content of the molecular species within the sample.}.
\MassON tackles both these problems in a fully Bayesian setting relying on a \textit{data augmentated} Gibbs sampling scheme.


\subsection*{Understanding Reaction Pathways} 

Another limitation of any mass spectrometer is the inability to tell apart substances with the same chemical formula but differing in their 3D structure.
In particular, this is the case of two post-translationally modified proteins that have the same modification that could be found on more than one residue. 
The spatial positioning of a modification is critical for the folding of the protein, and thus -- its function.
To position a PTM, one has to use more specific techniques, fragmentation being one of them. 
Ions can be fragmented either outside the instrument, via proteolytic digestion, or inside the instrument. 
Two prominent ways of inducing fragmentation inside the instrument are the Collisional Induced Dissociation (CID) and the Electron Transfer Dissociation (ETD).
The first one consists in heating up the sample cations by exposing them to collisions with some inert gas. 
This method produces more noisy spectra, as different parts of the molecule detach due to their increased internal motion.
ETD is much more subtle technique: it consists of an ion-ion reaction between the sample cations and anions, each carrying a radicals -- an electron in a higher energy state.
The meeting between these ions is deemed to result in four possible outcomes:
\begin{itemize}
  \item the transfer of electron from the anion to the cation resulting in the dissociation of the cation -- the proper ETD
  \item the transfer of electron that does not result in any dissociation -- ETnoD
  \item the ETD dissociation followed by a subsequent hydrogen transport -- HTR
  \item the proton transfer reaction -- PTR
\end{itemize}
To study the products of these fragmentation, we devised an approach named \MassTodon.
Chapter~\ref{chap::MassTodon} provides a detailed explanation of the approach we take to study these reactions.
The presented workflow can find the products of these reactions in the spectrum.
Furthermore, \MassTodoN can deconvolute the isotopic distributions of different products using constrained quadratic programming\footnote{The deconvolution performed by \MassTodoN is simpler than that presented in \MassOn. For this reason, \MassON will be described after \MassTodon.}.
It outputs the estimates of joint intensities of each chemical formula it found from the set of potential reaction products and substrates.
It can also estimate the probabilities with which different reactions occured in one experiment.
This simplifies the comparison of different mass spectra, offering a possibility to better study the influence of different instrumental settings upon the sample; finally, it also simplifies the comparison of different instruments.
In particular, \MassTodoN has already found its use to study the unfolding of proteins inside a mass spectrometer \citep{lermyte2017conformational}, as one can consider the odds ratio between the probabilities of two reactions taken into consideration (ETnoD and PTR).


\subsection*{Reaction Kinetics of Electron Transfer Reactions} 

With the estimates of the intensity of particular molecular species at hand, as provided by \MassTodon,
it seems natural to pose more specific questions about the nature of the chemical process that could result in a similar mass spectrum.
In Chapter~\ref{chap::ETDetective} \citep{ciach2017estimation}, we follow a natural approach in this context, which is to apply the well developed mathematical apparatus provided by the theory of reaction kinetics.

We have adapted an approach based on a dynamic stochastic Petri net proposed by \citet{Gambin2010}.
In the particular setting we study, the structure of that net reduces to a directed acyclic graph.
This fact significantly increases the theoretical tractability of the problem, as the chemical master equations can be directly applied to establish recursive formulas for the average numbers of ions across the net at a given time.
The solution to the above equations depends on a set of reaction rates, each specific for a different reaction.
By manipulating these parameters, we can thus compute theoretical numbers of ions and compare them with results obtained by \MassTodon.
We try to minimize the resulting error using a gradient-free L-BFGS-B algorithm.

The developed tool, called \ETDetectivE is fully integrated with \MassTodon.
Both algorithms are available for download for free.
We are also completing works on a web-service that will make the two algorithms available to a larger public.


\newpage
\subsection*{\hfil{Publications in Mass Spectrometry}} 

\begin{itemize}
  \item[] Lermyte, F., Łącki, M. K., Valkenborg, D., Gambin, A., \& Sobott, F. (2017). Conformational space and stability of ETD charge reduction products of ubiquitin. Journal of The American Society for Mass Spectrometry, 28(1), 69-76.
  \item[]  Lermyte, F., Łącki, M. K., Valkenborg, D., Baggerman, G., Gambin, A., \& Sobott, F. (2015). Understanding reaction pathways in top-down ETD by dissecting isotope distributions: A mammoth task. International Journal of Mass Spectrometry, 390, 146-154. 
  \item[] Ciach, M. A., Łącki, M. K., Miasojedow, B., Lermyte, F., Valkenborg, D., Sobott, F., \& Gambin, A. (2017, May). Estimation of Rates of Reactions Triggered by Electron Transfer in Top-Down Mass Spectrometry. In International Symposium on Bioinformatics Research and Applications (pp. 96-107). Accepted in the Journal of Computational Biology, doi: 10.1089/cmb.2017.0156.

  \item[] Łącki, M. K., Lermyte, F., Miasojedow, B., Startek, M., Sobott, F., \& Gambin, A. (2017). Assigning peaks and modeling ETD in top-down mass spectrometry. arXiv preprint arXiv:1708.00234. Submitted to the American Journal of Mass Spectrometry.
  \item[] Łącki, M. K., Startek, M., Valkenborg, D., \& Gambin, A. (2017). IsoSpec: Hyperfast Fine Structure Calculator. Analytical Chemistry, 89(6), 3272-3277.
\end{itemize}

\subsection*{\hfil{Other Publications}\hfil} 
\begin{itemize}
    \item[] Bielczyński, L.W., Łącki, M.K., Hoefnagels, I., Gambin, A., \& Croce, R. (2017). Leaf and plant age affects photosynthetic performance and photoprotective capacity. Conditionally accepted in Plant Physiology.
    \item[] Łącki, M. K., \& Miasojedow, B. (2016). State-dependent swap strategies and automatic reduction of number of temperatures in adaptive parallel tempering algorithm. Statistics and Computing, 26(5), 951-964.
\end{itemize}

\newpage
\subsection*{\hfil{Acknowledgements}\hfil} 

I would like to thank all the people that contributed to the creation of this thesis.
\\
\noindent I thank both my tutors: prof. Anna Gambin and dr Błażej Miasojedow.
\\
\noindent I thank my dear collaborators: Michał Ciach, Michał Startek, Frederik Lermyte, Frank Sobott, Dirk Valkenborg, Mikołaj Olszański, Ludwik Bielczyński, and, of course, Piotr Dittwald.
\\
\noindent I would also like to thank important institutions that supported me with their funding:
\begin{itemize}
  \item the National Science Centre for their financial support and entrusting me with grant number 2015/17/N/ST6/03565
  \item the Vlaamse Instelling voor Technologisch Onderzoek for funding the research of my collaborators in Belgium, without whose data we could not have done anything worthwhile, apart from \IsoSpec
\end{itemize}

\noindent Let me also thank my dearest parents, Agnieszka and Krzysztof, and my grandmother, Danuta, for their continuous support and understanding.

\noindent Finally, I would love to thank Yani Zhao, who must have gone through hell while I was writing all this. I am really sorry, honey! I will make it up to you!





 



% The functioning of a modern mass spectrometer consists of few stages  . 
% For as much as we are concerned in this disseration, these stages include the preparation of the sample, its ionization, filtration of ions, their fragmentation, and their final detection.

% \begin{figure}[h]
%     \includegraphics[width=\textwidth]{instrument_workflow.pdf}
% \end{figure}

% \newpage
% {\color{BrickRed}{\textbf{Samples}}}. 
% Mass spectrometers have been applied for various purposes throughout their history.
% At the beginning, they played a crucial role in the discovery and the determination of isotopes of different elements \citep{thomson1913bakerian}.
% Today, mass spectrometers used for these particular have been heavily specialized, and a whole subfield of Isotope-Ratio Mass Spectrometry is dedicated to that one task \citep{paul2007normalization}. Later on, mass spectrometry was typically used in geology to measure petroleum composition and perform carbon dating.

% In this work, we are mainly interested in the applications of mass spectrometry in the molecular biology. 
% In that context, it has been used to identify small molecules (such as metabolites), peptides and proteins (and their modifications), lipids, and short oligonucleotides.
% These achievements would not be possible without important improvements in the way the sample is introduced to the instrument in the late 80'ies. 
% These discoveries ultimately led to a Noble prize in chemistry in 2002 for John B. Fenn, the mind behind the Electrospray Ionization (ESI). 


% {\color{BrickRed}{\textbf{Ionization}}}. 
% To manipulate individual molecules with electric fields, one has to first assure that they are charged. 
% One of many possible ways of charging molecules consists in the attachment of protons in a process called Electrospray Ionization, or ESI for short.
% Without straying into the underpinning of the ionization process itself, let us mention that the ultimate result of ESI is the attachment of potentially multiple protons to some of the sample molecules.
% A proton has a positive charge and a mass only slightly smaller than that of the hydrogen atom, equal to 1.007 atomic mass units.
% The difference, i.e. the mass of the missing electron, can be neglected on most of the modern mass spectrometers, as it would be too small to resolve anyway. 
% A molecule with a mass $M$ and $k$ protons attached would have a mass of $M + k \times 1.007$ amu.
% Positively charged molecules, or cations, are introduced into the instrument.
% Already inside the instrument, they are being moved around and the small diffences in the electric field this movement incites are multiplied and recorded.

% {\color{BrickRed}{\textbf{Filtration}}}. 
% The trajectory of an ion inside the instrument depends on its mass-to-charge ratio.
% In all instruments, ions with lower mass travel faster than those with higher mass. 
% More charged ions also travel faster than those with a lesser number of charges. 
% The mass-to-charge ratio also influences the actual trajectory of ions inside the instrument.
% Influencing the trajectories of certain ions so that they do not arrive at the detector is the principle behind the filtering.
% Filtering ions with particular mass-to-charge is an important feature of these instruments.
% Filtering significantly reduces the number of observed ions, which usually simplifies the analysis of the obtained mass spectra. 
% It is also crucial for the purpose of screening, i.e. while focussing the research of substances with a  particular mass.




% Modern mass spectrometers can discern different molecules based on their mass-to-]



% harge ratio, $m/z$. 
% To give an example, consider butane \ce{C4H10} and butanol \ce{C4H9OH}. 
% Butane has a mass of roughly 58,1 atomic mass units (amu), and butanol -- around 74.1 amu.
% The instrument uses electric fields to control the movement of the molecules that are being introduced inside.
% Therefore, the molecules have to be charged, so that they react to the differences in the external potential. 
% One of many possible ways of charging molecules consists in the attachment of protons in a process called Electrospray Ionization, or ESI for short. 
% Suppose that both butane and butanol got one charge in ESI. 
% Each proton has only slightly smaller mass as any hydrogen atom, as the mass of electron is several orders of magnitude smaller than that of the proton.
% The mass of a proton is thus around 1 amu. 
% The mass-to-charge ratio of butane is simply $\frac{58.1 + 1}{1} = 59.1$; similarly, that of butanol is around $75.1$. 
% It is conventional to call the unit of $m/z$ a dalton, although that tradition has not been included into the SI standard of measures.
% The $16$ dalton difference between the molecules can be easily seen in the instrument.

% The instrument does not discern the signal of particular ions; rather than that, only large collections of ions with particular mass-to-charge ratio can be told apart. 



